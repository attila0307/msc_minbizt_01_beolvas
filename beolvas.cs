using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _20190212_beolvas
{
    class Beolvas
    {
        static void Main(string[] args)
        {
            int lineCount = File.ReadLines("beolvas.txt").Count();
            Console.WriteLine("A fajlban talalhato sorok szama: " + lineCount + "\nEbbol a rekordokat tartalmazo sorok: " + Math.Round(0.75*lineCount) + "\nSzamlak szama: " + Math.Round(0.25*lineCount) + "\n");

            string[] lines = File.ReadAllLines("beolvas.txt").ToArray();
            string[] ertekek = new string[9];
            string akt_ertek;
            int ert = 0;    //a 9 elemu tomb aktualis erteke
            int i = 0;
            int checksumval = 0;
            int checksummod = 0;
            bool illegal = false;


            foreach (string line in lines)
            {
                illegal = false;
                akt_ertek = null;
                checksumval = 0;
                checksummod = 0;

                if (i % 4 == 0 || i % 4 == 1 || i % 4 == 2)
                {
                    for (int j = 0; j < line.Length-2; j+=3)
                    {
                        //Console.Write(line[j]);
                        //Console.Write(line[j+1]);
                        //Console.Write(line[j+2]);
                        //Console.Write("\n");
                        ertekek[ert] = String.Concat(ertekek[ert], line[j], line[j+1], line[j+2]);
                        //Console.WriteLine(ertekek[ert]);
                        ert++;
                        if (ert >= 9)
                        {
                            ert = 0;
                        }
                    }

                    //Console.WriteLine("line");
                }

                else if (i % 4 == 3)
                {
                    for (ert = 0; ert < 9; ert++)
                    {
                        akt_ertek = ertekek[ert];
                        switch (akt_ertek)
                        {
                            case "     |  |":
                                ertekek[ert] = "1";
                                break;
                            case " _  _||_ ":
                                ertekek[ert] = "2";
                                break;
                            case " _  _| _|":
                                ertekek[ert] = "3";
                                break;
                            case "   |_|  |":
                                ertekek[ert] = "4";
                                break;
                            case " _ |_  _|":
                                ertekek[ert] = "5";
                                break;
                            case " _ |_ |_|":
                                ertekek[ert] = "6";
                                break;
                            case " _   |  |":
                                ertekek[ert] = "7";
                                break;
                            case " _ |_||_|":
                                ertekek[ert] = "8";
                                break;
                            case " _ |_| _|":
                                ertekek[ert] = "9";
                                break;
                            case " _ | ||_|":
                                ertekek[ert] = "0";
                                break;
                            default:
                                ertekek[ert] = "?";
                                illegal = true;
                                break;
                        }
                        Console.Write(ertekek[ert]);

                        if (illegal == false)
                        {
                            checksumval = checksumval + ((ert + 1) * int.Parse(ertekek[ert]));
                        }
                        
                    }
                    if (illegal == false)
                    {
                        checksummod = checksumval % 11;
                        if (checksummod != 0)
                        {
                            Console.Write(" ERR");
                        }

                        //Console.WriteLine("\nEllenorzoosszeg: " + checksumval + " mod 11 = " + checksummod);
                    }
                    else if (illegal == true)
                    {
                        Console.Write(" ILL");
                    }

                    Console.WriteLine();
                    Array.Clear(ertekek, 0, 9);    //kiuritem, a kovetkezo beolvasott szamsor erdekeben
                    ert = 0;    //a tomb elso elemere ugrik
                }
                i++;
            }

            Console.WriteLine("\nK�sz.");
            Console.ReadLine();

        }
    }
}
